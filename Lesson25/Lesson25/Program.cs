﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson25
{
    class Program
    {
        static void Main(string[] args)
        {

            RoundTable<Knight> knights = new RoundTable<Knight>();
            RoundTable<Magician> magicians = new RoundTable<Magician>();
            Knight k1 = new Knight();
            k1["Name"] = "lala";
            k1["Title"] = "master";
            k1["BirthTown"] = "camelot";
            knights.Add(k1);

            Knight k2 = new Knight();
            k1["Name"] = "baba";
            k1["Title"] = "leader";
            k1["BirthTown"] = "rome";
            knights.Add(k2);

            knights.RemoveAt(2);

            Knight k3 = new Knight();
            k3["Name"] = "ssdd";
            k3["Title"] = "ljasdk";
            k3["BirthTown"] = "sdfsdf";

            knights.InsertAt(3, k3);

            knights.GetRounded(6);

            Magician m1 = new Magician();
            m1["Name"] = "lala";
            m1["Title"] = "master";
            m1["FavoriteSpell"] = "lightning";
            magicians.Add(m1);

            Magician m2 = new Magician();
            m2["Name"] = "papa";
            m2["Title"] = "leader";
            m2["FavoriteSpell"] = "fire";
            magicians.Add(m2);

            magicians.RemoveAt(1);

            Magician m3 = new Magician();
            m3["Name"] = "papa";
            m3["Title"] = "leader";
            m3["FavoriteSpell"] = "fire";

            magicians.InsertAt(3, m3);

            magicians.GetRounded(4);
        }
    }
}
