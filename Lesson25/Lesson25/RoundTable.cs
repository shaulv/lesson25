﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson25
{
    public class RoundTable<T> : IEnumerable<T>, IComparable<T>
    {
        #region Properties
        List<T> entities = new List<T>();
        #endregion

        #region Methods
        public void Add(T a)
        {
            entities.Add(a);
        }
        public void RemoveAt(int i)
        {
            if (entities.Count == 0)
                return;

            entities.RemoveAt(i % entities.Count);
        }
        public void InsertAt(int i, T item)
        {
            entities.Insert(i % entities.Count, item);
        }
        public void Clear()
        {
            entities.Clear();
        }
        public void Sort()
        {
            entities.Sort();
        }
        public List<T> GetRounded(int num)
        {
            List<T> returnList = new List<T>();

            int counter = 0;
            int index = 0;
            while (counter++ < num)
            {
                returnList.Add(entities[index++]);
                if (index >= entities.Count)
                    index = 0;
            }

            return returnList;
        }
        public T this[int index]
        {
            get
            {
                if (entities.Count == 0)
                    return default(T);
                return entities[index % entities.Count];
            }
        }
        public T this[string name]
        {
            get
            {
                if (entities.Count == 0)
                    return default(T);

                foreach (T entity in entities)
                {
                    if (entity.Name == name)
                        return entity;
                }

                return default(T);
            }
        }
        public IEnumerator<T> GetEnumerator()
        {
            return entities.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return entities.GetEnumerator();
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public int CompareTo(T other)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
