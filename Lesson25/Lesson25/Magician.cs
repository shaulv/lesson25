﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson25
{
    public class Magician
    {
        #region Properties
        public string Name { get; private set; }
        public string BirthTown { get; private set; }
        public string FavoriteSpell { get; private set; }
        #endregion

        #region Methods
        public string this[string fieldName]
        {
            get
            {
                switch (fieldName)
                {
                    case "Name":
                        return this.Name;
                    case "BirthTown":
                        return this.BirthTown;
                    case "FavoriteSpell":
                        return this.FavoriteSpell;
                }
                return null;
            }
            set
            {
                switch (fieldName)
                {
                    case "Name":
                        this.Name = value;
                        break;
                    case "BirthTown":
                        this.BirthTown = value;
                        break;
                    case "FavoriteSpell":
                        this.FavoriteSpell = value;
                        break;
                }
            }
        }
        public override string ToString()
        {
            return $"Name: {Name}\nBirthTown: {BirthTown}\nTitle: {FavoriteSpell}\n";
        }
        #endregion
    }
}
